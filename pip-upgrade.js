#!/usr/bin/env node

const colors = {
  red: str => `\u001b[31m${str}\u001b[0m`,
  green: str => `\u001b[32m${str}\u001b[0m`,
  yellow: str => `\u001b[33m${str}\u001b[0m`,
  blue: str => `\u001b[34m${str}\u001b[0m`,
};

const exec = require("child_process").exec;
const pipFreeze = "pip freeze";

exec(pipFreeze, (err, stdout, stderr) => {
  const arr = stdout.trim().split("\n");

  arr.forEach(obj => {
    const [o, v] = obj.split("==");
    const pipInstallUpgrade = `pip install ${o} --upgrade`;

    console.log(`${colors.blue("Checking updates for ")}${colors.yellow(o)} ${colors.blue("(current: ")}${colors.yellow(v)}${colors.blue(")...")}`);

    exec(pipInstallUpgrade, (err, stdout, stderr) => {
      if (err) {
        throw err;
      }

      if (stdout) {
        console.log(colors.green(stdout));
      } else {
        console.log(colors.red(stderr));
      }
    });
  });
});
